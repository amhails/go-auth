package main

import (
	"encoding/base64"
	"log"
	"net/http"
	"os"

	"gopkg.in/authboss.v0"
	_ "gopkg.in/authboss.v0/auth"

	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"github.com/justinas/alice"
	"github.com/justinas/nosurf"
)

var (
	ab       = authboss.New()
	database = NewMemStorer()
)

func setupAuthboss() {
	ab.Storer = database
	ab.MountPath = "/auth"
	ab.RootURL = `http://localhost:3000`

	ab.XSRFName = "csrf_token"
	ab.XSRFMaker = func(_ http.ResponseWriter, r *http.Request) string {
		return nosurf.Token(r)
	}

	ab.CookieStoreMaker = NewCookieStorer
	ab.SessionStoreMaker = NewSessionStorer

	if err := ab.Init(); err != nil {
		log.Fatal(err)
	}
}

func main() {

	cookieStoreKey, _ := base64.StdEncoding.DecodeString(`NpEPi8pEjKVjLGJ6kYCS+VTCzi6BUuDzU0wrwXyf5uDPArtlofn2AG6aTMiPmN3C909rsEWMNqJqhIVPGP3Exg==`)
	sessionStoreKey, _ := base64.StdEncoding.DecodeString(`AbfYwmmt8UCwUuhd9qvfNA9UCuN1cVcKJN1ofbiky6xCyyBj20whe40rJa3Su0WOWLWcPpO1taqJdsEI/65+JA==`)
	cookieStore = securecookie.New(cookieStoreKey, nil)
	sessionStore = sessions.NewCookieStore(sessionStoreKey)

	setupAuthboss()

	mux := mux.NewRouter()

	gets := mux.Methods("GET").Subrouter()
	mux.PathPrefix("/auth").Handler(ab.NewRouter())
	gets.Handle("/list", authProtect(list))
	gets.HandleFunc("/", login)

	stack := alice.New(logger, nosurfing, ab.ExpireMiddleware).Then(mux)

	// Start the server
	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "3000"
	}

	log.Println(http.ListenAndServe("localhost:"+port, stack))
}

func list(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func login(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusUnauthorized)
}
